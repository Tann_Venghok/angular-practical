import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { NgbDatepickerMonth } from '@ng-bootstrap/ng-bootstrap';
import { validateConfirmPassword,checkIfMatchingPasswords } from '../validate-password.validator';
import { checkBoxValidator } from '../checkBox-validator.validator';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css'],
})
export class RegistrationFormComponent implements OnInit {
isSubmit = false;
  constructor(private fb: FormBuilder) {}

  registrationForm = this.fb.group(
    {
      firstname: ['', [Validators.required,
        Validators.maxLength(32)]],
      lastname: ['', [
        Validators.required,
        Validators.maxLength(32),
      ]],
      email: ['', [Validators.required, Validators.email]],
      areaCode:['', [
        Validators.required,
        Validators.pattern('^[0-9]+$'),
      ]],
      // parentPassword: new FormGroup({
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(128),
      ]],
      confirmPassword: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(128),
      ]],
      // }, { validators: matchingPasswordValidator }),
      phoneNumber: ['', [
        Validators.required,
        Validators.pattern('^[0-9]+$'),
      ]],

      streetAddress: ['', [Validators.required]],
      streetAddress2:['', [Validators.required]],
      city: ['', [Validators.required]],
      stateProvince: ['', [Validators.required]],
      zipCode: ['', [
        Validators.required,
        Validators.pattern('^[0-9]+$'),
      ]],
      date:['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      country: ['', Validators.required],
      // myCheckboxGroup: new FormGroup({
      //   myCheckbox1: new FormControl(false),
      //   myCheckbox2: new FormControl(false),
      //   myCheckbox3: new FormControl(false),
      // }, checkBoxValidator()),
      cbGroup: new FormGroup(
        {
          friendCB: new FormControl(false),
          googleCB: new FormControl(false),
          blogPostCB: new FormControl(false),
          NewsArticleCB: new FormControl(false)
        },
        // checkBoxValidator()
        Validators.requiredTrue
      ),
      membershipRuleCB: ['', [Validators.requiredTrue]],
      privacyPolicyCB: ['', [Validators.requiredTrue]],
    }
    ,
    {
      validator: checkIfMatchingPasswords('password', 'confirmPassword')
    }
  );

  dateNumber: any = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
  ];
  monthName: any = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  yearNumber: any = [
    2002,
    2001,
    2000,
    1999,
    1998,
    1997,
    1996,
    1995,
    1994,
    1993,
    1992,
    1991,
    1990,
  ];

  countryName: any =['Cambodia', 'USA', 'Japan','China'];

  get checkBoxGroup(){
    return this.registrationForm.get('cbGroup');
  }

  get privacyPolicyCB() {
    return this.registrationForm.get('privacyPolicyCB');
  }

  get membershipRuleCB() {
    return this.registrationForm.get('membershipRuleCB');
  }
  get parentPassword() {
    return this.registrationForm.get('parentPassword');
  }
  get country() {
    return this.registrationForm.get('country');
  }
  get month() {
    return this.registrationForm.get('month');
  }

  get date() {
    return this.registrationForm.get('date');
  }

  get year() {
    return this.registrationForm.get('year');
  }
  get streetAddress() {
    return this.registrationForm.get('streetAddress');
  }

  get streetAddress2() {
    return this.registrationForm.get('streetAddress2');
  }

  get city() {
    return this.registrationForm.get('city');
  }

  get stateProvince() {
    return this.registrationForm.get('stateProvince');
  }

  get zipCode() {
    return this.registrationForm.get('zipCode');
  }

  get firstname() {
    return this.registrationForm.get('firstname');
  }

  get lastname() {
    return this.registrationForm.get('lastname');
  }

  get email() {
    return this.registrationForm.get('email');
  }

  get areaCode() {
    return this.registrationForm.get('areaCode');
  }

  get phoneNumber() {
    return this.registrationForm.get('phoneNumber');
  }

  get password() {
    return this.registrationForm.get('password');
  }

  get confirmPassword() {
    return this.registrationForm.get('confirmPassword');
  }

  ngOnInit(): void {}

  onSubmit() {
    this.isSubmit = true;
    console.log(this.registrationForm.value);
    console.log("hi")
  }
}



