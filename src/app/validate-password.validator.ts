import {
  AbstractControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

export function validateConfirmPassword(
  controlName: string,
  matchingControlName: string
): ValidatorFn {
  return (formGroup: AbstractControl): ValidationErrors | null => {
    const password = formGroup.get(['controlName']);
    const confirmPassword = formGroup.get(['matchingControlName']);

    if (password.value != confirmPassword.value) {
      return { mismatchPassword: true };
    } else return null;
  };
}

export function checkIfMatchingPasswords(
  passwordKey: string,
  passwordConfirmationKey: string
) {
  return (group: FormGroup) => {
    let passwordInput = group.controls[passwordKey],
      passwordConfirmationInput = group.controls[passwordConfirmationKey];
    if (passwordInput.value !== passwordConfirmationInput.value) {
      return passwordConfirmationInput.setErrors({ mismatchPassword: true });
    } else {
      return passwordConfirmationInput.setErrors(null);
    }
  };
}
